import { CliAngularEducCssPage } from './app.po';

describe('cli-angular-educ-css App', () => {
  let page: CliAngularEducCssPage;

  beforeEach(() => {
    page = new CliAngularEducCssPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
